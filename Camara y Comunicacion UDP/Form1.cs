﻿
#region USING

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using System.Net;                               //Para UDP
using System.Net.Sockets;                       //Para UDP

using System.Windows.Forms;
using VisioForge.Types.OutputFormat;            //Para grabar video en PC
using AForge.Video.DirectShow;                  //Para visualizar camara
using AForge.Video;                             //Para visualizar camara
using Accord.Video.VFW;

using Excel = Microsoft.Office.Interop.Excel;   //Para Excel

#endregion

namespace Camara_y_Comunicacion_UDP
{
    public partial class Form1 : Form
    {

        #region DECLARACIONES
        int i = 0;
        int tiempo_ensayo;
        string[] dato_recibido = new string[1000];
        private string ip_arduino = "192.168.1.109";
        private int port_arduino = 8080;

        UdpClient udpClient_Datos = new UdpClient();
        string Aux_Nombre_Viejo = "";

        int Finalizacion_Forzada = 0;
        Excel.Application excel = new Excel.Application();
        private Dictionary<string, int> Txns = new Dictionary<string, int>();
      

        #endregion


        #region GENERAL
        public Form1()
        {
            InitializeComponent();
        }

        public void Form1_Load(object sender, EventArgs e)
        {
            Thread thdUDPServer = new Thread(new ThreadStart(Server_Thread));
            thdUDPServer.Start();
            Iniciar_Camara_IP();
            releaseObject(excel);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //udpClient_Datos.Close();
            timer_actual.Stop();
            //videoCapture1.Stop();
            //excel.UserControl = false;
            //excel.Quit();
            //releaseObject(excel);

            //    GC.Collect();
            //   Application.ExitThread();
            //this.Dispose();
            //Application.ExitThread();


            //GC.SuppressFinalize(this);
            //    GC.Collect();

            //this.Close();
            Application.ExitThread();
            Application.Exit();
            Environment.Exit(0);
        }

        private void Form1_FormClosed()
        {
            udpClient_Datos.Close();
            timer_actual.Stop();
            videoCapture1.Stop();
            excel.UserControl = false;
            excel.Quit();
            releaseObject(excel);
            //Application.Exit();
            Application.ExitThread();
            this.Close();
            this.Dispose();
            GC.SuppressFinalize(this);
        }
        #endregion


        #region COMUNICACIÓN UDP
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////---UDP---////////////////////////////////////////////////////

        public void Server_Thread()
        {
            udpClient_Datos = new UdpClient(port_arduino);
            while (true)
            {
                IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, port_arduino);
                Byte[] receiveBytes = udpClient_Datos.Receive(ref RemoteIpEndPoint);
                string returnData = Encoding.ASCII.GetString(receiveBytes);

                this.Invoke(new MethodInvoker(delegate () //BORRAR, ES SOLO VISUALIZACION
                {
                    listBox_received.Items.Add(returnData.ToString());//BORRAR, ES SOLO VISUALIZACION
                    listBox_received.SelectedIndex = listBox_received.Items.Count - 1;//BORRAR, ES SOLO VISUALIZACION
                    listBox_received.SelectedIndex = -1;//BORRAR, ES SOLO VISUALIZACION

                })); //BORRAR, ES SOLO VISUALIZACION

                int Cases = Int32.Parse(returnData);

                switch (Cases)
                {
                    case 1111:

                        Finalizacion_Forzada = 0;
                        Iniciar_Timer();
                        Iniciar_Grabacion_IP();
                        Enviar_Tipo_y_Duracion();

                        if (textBox_Raton.InvokeRequired)           //Limpio el texto al iniciar, para q se den cuenta de ingresar el nombre del raton
                        {
                            textBox_Raton.Invoke(new MethodInvoker(delegate {textBox_Raton.Clear();
                            }));
                        }

                        for (int j = 0; j < 1000; j++)  //Pongo a Cero todo el vector por si quedaron valores "1111" viejos
                        {
                            dato_recibido[j] = null;
                        }

                        break;

                    case 9999:

                        Detener_Timer();
                        Detener_Grabacion_IP();
                        i = 0;
                        Recibir_Datos_Excel(RemoteIpEndPoint, returnData);
                        Thread.Sleep(200);
                        Crea_Excel(dato_recibido, i);

                        break;

                    case 8888:

                        Detener_Timer();
                        Detener_Grabacion_IP();
                        i = 0;

                        break;

                    default:

                        break;

                }
            }
        }

        public void Enviar_Tipo_y_Duracion()
        {
            string tipo_ensayo = "";
            Thread.Sleep(200);                                              //con este retraso me aseguro de que la primera vez llega la info
            UdpClient udpClient_Duracion = new UdpClient();
            udpClient_Duracion.Connect(ip_arduino, port_arduino);

            Byte[] senddata_tiempo = Encoding.ASCII.GetBytes(textBox_Duracion.Text);
            udpClient_Duracion.Send(senddata_tiempo, senddata_tiempo.Length);

            udpClient_Duracion.Close();

            if (comboBox_Ensayo.InvokeRequired)
            {
                comboBox_Ensayo.Invoke(new MethodInvoker(delegate {
                    tipo_ensayo = comboBox_Ensayo.SelectedIndex.ToString();
                }));    //si fue ejecutado desde el case, lo invoca
            }
            else
            {
                tipo_ensayo = comboBox_Ensayo.SelectedIndex.ToString();         // si fue ejecutado con el click de send data, le manda esto
            }

            Thread.Sleep(200);
            UdpClient udpClient_Tipo = new UdpClient();
            udpClient_Tipo.Connect(ip_arduino, port_arduino);

            Byte[] senddata_tipo = Encoding.ASCII.GetBytes(tipo_ensayo);
            udpClient_Tipo.Send(senddata_tipo, senddata_tipo.Length);

            udpClient_Tipo.Close();
        }


        private void button_Clear_Click(object sender, EventArgs e)
        {
            listBox_received.Items.Clear();
        }

        public void button_Send_Data_Click(object sender, EventArgs e)
        {
            Enviar_Tipo_y_Duracion();
        }

        #endregion


        #region LECTURA Y GENERACIÓN DE EXCEL
        //////////////////////LECTURA DE DATOS PARA EXCEL///////////////////////////////////////////////////////////

        public void Recibir_Datos_Excel(IPEndPoint RemoteIpEndPoint, string returnData)
        {

            while (returnData != "7777")
            {
                Byte[] receiveBytes = udpClient_Datos.Receive(ref RemoteIpEndPoint);
                returnData = Encoding.ASCII.GetString(receiveBytes);

                dato_recibido[i] = returnData;

                this.Invoke(new MethodInvoker(delegate ()
                {
                    listBox_received.Items.Add(returnData.ToString());
                    listBox_received.SelectedIndex = listBox_received.Items.Count - 1;
                    listBox_received.SelectedIndex = -1;

                    i++;
                }));
                
            }
        }


        /////////////////////////////////////---Grabar Excel en PC---///////////////////////////////////////////////
        public void Crea_Excel(string[] data, int cant)
        {
            string tipo_ensayo="";
            Excel.Application excel = new Excel.Application();
            Excel._Workbook libro = null;
            Excel._Worksheet hoja = null;

            //creamos un libro nuevo y la hoja con la que vamos a trabajar
            libro = (Excel._Workbook)excel.Workbooks.Add(Excel.XlWBATemplate.xlWBATWorksheet);
            hoja = (Excel._Worksheet)libro.Worksheets.Add();

            if (comboBox_Ensayo.InvokeRequired)
            {
                comboBox_Ensayo.Invoke(new MethodInvoker(delegate { tipo_ensayo = comboBox_Ensayo.Text; }));
            }

            hoja.Name = tipo_ensayo;                                            //Le pongo el nombre del tipo de ensayo, desp vemos que queda.
            ((Excel.Worksheet)excel.ActiveWorkbook.Sheets["Hoja1"]).Delete();   //Borramos la hoja que crea en el libro por defecto

            //Montamos las cabeceras 
            Armar_Cabecera(ref hoja);

            //Rellenamos las celdas
            int fila = 2;
            for (int i = 0; i < cant/2; i++)
            {
                //Asignamos los datos a las celdas de la fila
                hoja.Cells[fila + i, 1] = data[2*i];
            }

            for (int i = 0; i < cant/2; i++)
            {
                //Asignamos los datos a las celdas de la fila
                hoja.Cells[fila + i, 2] = data[2*i+1];
            }


            libro.Saved = true;
            libro.SaveAs((Environment.GetFolderPath(Environment.SpecialFolder.Desktop)) + "\\Documentos Ensayos\\" +
            (DateTime.Now.ToString("yyyy")) + (DateTime.Now.ToString("MM")) + (DateTime.Now.ToString("dd")) +
            DateTime.Now.ToString("_HHmmss_") + tipo_ensayo + "_" + textBox_Raton.Text + ".xlsx");
            //libro.SaveAs(Environment.CurrentDirectory + @"\Ejemplo.xlsx");  // Si es un libro nuevo

            libro.Close();
            releaseObject(libro);
            
            excel.UserControl = false;
            excel.Quit();
            releaseObject(excel);
            //excel.Application.Quit();
            //System.Runtime.InteropServices.Marshal.ReleaseComObject(excel.Application);
            


            for (int j=0; j<1000; j++)
            {
                dato_recibido[j] = null;
            }

        }

        private void Armar_Cabecera(ref Excel._Worksheet hoja)
        {
            
                Excel.Range rango;

                //** Pongo titulos en fila 1 **
                hoja.Cells[1, 1] = "# Hole";
                hoja.Cells[1, 2] = "Time [seg]";

                //Centramos los textos
                rango = hoja.Rows[1];
                rango.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                //Modificamos los anchos de las columnas
                rango = hoja.Columns[1];
                rango.ColumnWidth = 14;
                rango = hoja.Columns[2];
                rango.ColumnWidth = 14;
            }

        private void releaseObject (object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        #endregion


        #region TIEMPO RESTANTE

        private void Iniciar_Timer()
        {
            Thread Thread_Timer = new Thread(delegate ()
            {
                // Invoke your control like this
                this.Invoke(new MethodInvoker(delegate ()
                {
                    if (timer_actual.Enabled == false)
                    {
                        //timer_actual = new System.Windows.Forms.Timer();
                        timer_actual.Tick += new EventHandler(timer_actual_Tick);
                        //timer_actual.Interval = 1000; // 1 second
                        timer_actual.Start();

                        if (textBox_Duracion.InvokeRequired)
                        {
                            textBox_Duracion.Invoke(new MethodInvoker(delegate { tiempo_ensayo = Int32.Parse(textBox_Duracion.Text); }));
                        }

                        tiempo_ensayo = Int32.Parse(textBox_Duracion.Text);
                    }
                }));
            });
            Thread_Timer.Start();
        }


        private void Detener_Timer()
        {
            timer_actual.Stop();
            //timer_actual.Dispose();
            timer_actual.Tick -= timer_actual_Tick;
        }


        private void timer_actual_Tick(object sender, EventArgs e)
        {
            tiempo_ensayo--;
            label_Tiempo.Text = Convert.ToString(tiempo_ensayo) + " seg";
            if (tiempo_ensayo == 0)
            {
                //timer_actual.Stop();
            }
        }

        #endregion


        #region INICIO Y GRABACION CAMARA

        public void Iniciar_Camara_IP()
        {
            int CAM_IP = 1;
            int CAM_DROID = 0;

            Thread Thread_Camara = new Thread(delegate ()
            {
                // Invoke your control like this
                this.Invoke(new MethodInvoker(delegate ()
                {
                    if (CAM_IP == 1)
                    {
                        videoCapture1.IP_Camera_Source = new VisioForge.Types.Sources.IPCameraSourceSettings()
                        { URL = "rtsp://admin:admin1234@192.168.1.108:554/cam/realmonitor?channel=1&subtype=0" };

                        videoCapture1.Audio_PlayAudio = videoCapture1.Audio_RecordAudio = false;

                        //videoCapture1.Video_ResizeOrCrop_Enabled = true;
                        
                        //videoCapture1.Video_Resize = new VisioForge.Types.VideoResizeSettings   //Se agrega para cambiar la resolucion a 640x480 
                        //{                                                                       //pero manteniendo la buena resolucion de la camara
                        //    Width = 640, Height = 480, LetterBox = true
                        //};

                        videoCapture1.Output_Format = new VFMP4v8v10Output(); //using MP4 output with default settings
                        videoCapture1.Mode = VisioForge.Types.VFVideoCaptureMode.IPCapture;
                    }

                    if (CAM_DROID == 1)
                    {
                        videoCapture1.Audio_PlayAudio = videoCapture1.Audio_RecordAudio = false;
                        videoCapture1.Video_CaptureDevice = videoCapture1.Video_CaptureDevicesInfo[1].Name; //es la droid camm [1] //es la webcam [0]

                        videoCapture1.Output_Format = new VFMP4v8v10Output(); //using MP4 output with default settings
                        //videoCapture1.Output_Format = new VFAVIOutput(); // defult AVI output with MJPEG for video and PCM for audio
                        videoCapture1.Mode = VisioForge.Types.VFVideoCaptureMode.VideoCapture;
                    }

                }));
            });
            Thread_Camara.Start();
            
        }

        public void Iniciar_Grabacion_IP()
        {
            string tipo_ensayo = "";

            if (comboBox_Ensayo.InvokeRequired)
            {
                comboBox_Ensayo.Invoke(new MethodInvoker(delegate { tipo_ensayo = comboBox_Ensayo.Text; }));
            }

            Aux_Nombre_Viejo = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\Documentos Ensayos\\" +
            (DateTime.Now.ToString("yyyy")) + (DateTime.Now.ToString("MM")) + (DateTime.Now.ToString("dd")) + "_" + "video_temporal.mp4";

            videoCapture1.Output_Filename = Aux_Nombre_Viejo;
            
            videoCapture1.Start();
        }


        public void Detener_Grabacion_IP()
        {
            string tipo_ensayo = "";
            string Aux_Nombre_Nuevo = "";

            

            if (comboBox_Ensayo.InvokeRequired)
            {
                comboBox_Ensayo.Invoke(new MethodInvoker(delegate { tipo_ensayo = comboBox_Ensayo.Text; }));
            }

            Aux_Nombre_Nuevo = (Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\Documentos Ensayos\\" +
            (DateTime.Now.ToString("yyyy")) + (DateTime.Now.ToString("MM")) + (DateTime.Now.ToString("dd")) +
            DateTime.Now.ToString("_HHmmss_") + tipo_ensayo + "_" + textBox_Raton.Text + "_" + "video.mp4");

            videoCapture1.Stop();

            if (Finalizacion_Forzada == 0) {        //Pongo esto aca para que si finalizó forzado, no cambie el nombre ya que no lo encontraria
                System.IO.File.Move(Aux_Nombre_Viejo, Aux_Nombre_Nuevo);
            }


        }


        #endregion


        #region FINALIZACION ENSAYO

        private void button_Finalizar_Ensayo_Click(object sender, EventArgs e)
        {
            Detener_Timer();
            Detener_Grabacion_IP();
            i = 0;
            label_Tiempo.Text = "0 seg";
            Finalizacion_Forzada = 1;

        }

        #endregion
    }
}